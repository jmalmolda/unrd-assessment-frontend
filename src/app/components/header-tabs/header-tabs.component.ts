import { Component, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-header-tabs',
  templateUrl: './header-tabs.component.html',
  styleUrls: ['./header-tabs.component.css'],
})
export class HeaderTabsComponent implements OnInit {

  @Input() tab: number

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  navigate(location: string) {
    this.router.navigate([location])
  }
}
