import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { ApiService } from '../../service/api.service'
import { UserCriteriaValue } from '../../model/userCriteriaValue.model'
import { CriteriaType } from 'src/app/model/criteriaType.model'
import { find } from 'lodash-es'

@Component({
  selector: 'app-user-features',
  templateUrl: './user-features.component.html',
  styleUrls: ['./user-features.component.css'],
})
export class UserFeaturesComponent implements OnInit {

  allValues: Array<{ criteria: CriteriaType, value: string }>

  constructor(private router: Router, private apiService: ApiService) { }

  private async getData() {
    const res = await this.apiService.getCriteriaTypes().toPromise()
    this.allValues = res.map(x => ({ criteria: x, value: '' }))

    for (const item of this.allValues) {
      item.criteria.values = await this.apiService.getCriteriaValues(item.criteria.id).toPromise()
    }

    const userId = window.localStorage.getItem('userId')

    const userCriteria = await this.apiService.getUserCriteria(userId).toPromise()

    for (const value of userCriteria) {
      const type = find(this.allValues,
        c => find(c.criteria.values, v => v.id === value.criteria_value_id))

      if (type) {
        type.value = value.criteria_value_id
      }

    }
    console.log('all values', this.allValues)

  }

  ngOnInit(): void {
    if (!window.localStorage.getItem('token')) {
      this.router.navigate(['login'])
      return
    }
    this.getData()
  }

  navigateBack() {
    this.router.navigate(['list-user'])
  }

  onSubmit() {
    console.log('allValues', this.allValues)
    for (const item of this.allValues) {
      this.apiService.saveUserCriteria(window.localStorage.getItem('userId'),
      item.value)
        .subscribe(_ => {
        })
    }
    alert('values saved to DB')
  }

}
